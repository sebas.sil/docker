#######################################################
FROM redhat/ubi8 AS builder
USER root
RUN yum repolist
RUN yum -y update
RUN echo 'root:welcome1' | chpasswd

#######################################################
FROM builder AS weblogic14
USER root
RUN yum -y install unzip

RUN mkdir -p /scratch/u01
RUN mkdir -p /app
RUN useradd weblogic
RUN chown -R weblogic:weblogic /scratch
RUN chown -R weblogic:weblogic /app
USER weblogic
WORKDIR /scratch/u01/

COPY fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip /scratch/u01/
RUN unzip fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip
RUN rm fmw_14.1.1.0.0_wls_lite_Disk1_1of1.zip

COPY jdk-8u371-linux-x64.tar.gz /scratch/u01/
RUN tar -zxvf jdk-8u371-linux-x64.tar.gz
RUN rm jdk-8u371-linux-x64.tar.gz

ENV JAVA_HOME=/scratch/u01/jdk1.8.0_371
ENV PATH=$PATH:$JAVA_HOME/bin

COPY wls.rsp /scratch/u01/
COPY oraInst.loc /scratch/u01/
COPY silent.xml /scratch/u01/
COPY wlst_commands.txt /scratch/u01/
RUN java -Xmx1024m -jar fmw_14.1.1.0.0_wls_lite_generic.jar -silent -responseFile /scratch/u01/wls.rsp -invPtrLoc /scratch/u01/oraInst.loc

ENV MW_HOME=/app/oracle/FMW
ENV WL_HOME=$MW_HOME/wlserver
ENV ORACLE_COMMON_HOME=$MW_HOME/oracle_common
ENV ORACLE_HOME=$MW_HOME/wlserver

RUN /app/oracle/FMW/oracle_common/common/bin/commEnv.sh
RUN /app/oracle/FMW/oracle_common/common/bin/wlst.sh < /scratch/u01/wlst_commands.txt
