# Weblogic 14 docker image with silent install

1. git clone

  git@gitlab.com:sebas.sil/docker.git

2. Download Docker - https://www.docker.com/products/docker-desktop/

3. Download weblogic - http://www.oracle.com/technetwork/middleware/weblogic/downloads/index.html

  copy the file to *docker git created folder*

4. Download java8 - https://download.oracle.com/otn/java/jdk/8u371-b11/ce59cff5c23f4e2eaf4e778a117d4c5b/jdk-8u371-linux-x64.tar.gz

  copy the file to *docker git created folder*

5. install docker

6. install docker image - https://hub.docker.com/r/redhat/ubi8

  cd *docker git created folder*
  docker-compose -f docker-compose-wls.yml build
  docker-compose -f docker-compose-wls.yml up


- docker-compose.yml: docker container configuration
- Dockerfile: docker configuration and image build instructions
- oraInst.loc: oracle inventary to silent wls instalation
- wls.rsp: response file to silent wls instalation
- wlst_commands.txt: wlst commands to wls configuration
+ silent.xml: furter silent configuration to wls domain configuration
